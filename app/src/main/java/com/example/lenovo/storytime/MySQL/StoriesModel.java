package com.example.lenovo.storytime.MySQL;


public class StoriesModel {

    private int story_id;
    private String title;
    private String story_cover_img;
    private String story_img;
    private String story_content;
    private String category;
    private String uploaded_at;
    private String downloaded;

    public StoriesModel(int story_id, String title, String story_cover_img, String story_img, String story_content, String category, String uploaded_at, String downloaded) {
        this.story_id = story_id;
        this.title = title;
        this.story_cover_img = story_cover_img;
        this.story_img = story_img;
        this.story_content = story_content;
        this.category = category;
        this.uploaded_at = uploaded_at;
        this.downloaded = downloaded;
    }

    public int getStory_id() {
        return story_id;
    }

    public void setStory_id(int story_id) {
        this.story_id = story_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory_cover_img() {
        return story_cover_img;
    }

    public void setStory_cover_img(String story_cover_img) {
        this.story_cover_img = story_cover_img;
    }

    public String getStory_img() {
        return story_img;
    }

    public void setStory_img(String story_img) {
        this.story_img = story_img;
    }

    public String getStory_content() {
        return story_content;
    }

    public void setStory_content(String story_content) {
        this.story_content = story_content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUploaded_at() {
        return uploaded_at;
    }

    public void setUploaded_at(String uploaded_at) {
        this.uploaded_at = uploaded_at;
    }

    public String getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(String downloaded) {
        this.downloaded = downloaded;
    }
}
