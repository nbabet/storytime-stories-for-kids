package com.example.lenovo.storytime.LoginandRegister;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.storytime.Connections.CheckNetwork;
import com.example.lenovo.storytime.Connections.ipaddress;
import com.example.lenovo.storytime.MySQL.StoriesActivity;
import com.example.lenovo.storytime.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private EditText et_readername,et_password;
    private Button btn_login;
    private TextView tv_register,tv_signin;
    private RequestQueue requestQueue;
    private static final String folder_file = "/Login.php";
    private StringRequest request;
    CheckBox cb;
    ProgressDialog progressDialog;
    ipaddress ipaddress = new ipaddress();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(SharedPrefManager.getmInstance(getApplicationContext()).isLogged()){
            startActivity(new Intent(LoginActivity.this,StoriesActivity.class));
            finish();
        }
        if(CheckNetwork.isInternetAvailable(LoginActivity.this)) //returns true if internet available
        {
        }
        else
        {
            showDialognoConnection();
        }

        overridePendingTransition(R.anim.slide, R.anim.slideout);
        et_readername = (EditText)findViewById(R.id.et_readername);
        et_password = (EditText)findViewById(R.id.et_password);
        btn_login = (Button)findViewById(R.id.btn_login);
        tv_register = (TextView)findViewById(R.id.tv_register);
        requestQueue = Volley.newRequestQueue(this);
        tv_signin = (TextView)findViewById(R.id.tv_signin);
        cb = (CheckBox)findViewById(R.id.cb);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KBZipaDeeDooDah.ttf");
        tv_register.setTypeface(font);
        tv_signin.setTypeface(font);
        btn_login.setTypeface(font);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked) {
                    et_password.setInputType(129);

                } else {
                    et_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetwork.isInternetAvailable(LoginActivity.this)) //returns true if internet available
                {

                    login(et_readername.getText().toString(),et_password.getText().toString());

                }
                else
                {
                    showDialognoConnection();
                }

            }
        });
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent (LoginActivity.this, RegisterActivity.class);
                startActivity(in);
                finish();
            }
        });
        progressDialog = new ProgressDialog(this);
    }

    private void login(final String readername,final String password){
        if (TextUtils.isEmpty(readername)) {
            et_readername.setError("Please enter username");
            et_readername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            et_password.setError("Please enter password");
            et_password.requestFocus();
            return;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ipaddress.ip_address + folder_file,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (!jsonObject.getBoolean("error")) {
                                SharedPrefManager.getmInstance(getApplicationContext()).userLogin(
                                        jsonObject.getString("id"),
                                        jsonObject.getString("readername"),
                                        jsonObject.getString("password")
                                );

                                showDialogLoggedin();

                            }else {

                                Toast.makeText(getApplicationContext(), "Already logged in"+jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),response, Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }catch (Exception e) {
                            Toast.makeText(getApplicationContext(),response, Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("readername", readername);
                params.put("password", password);


                return params;
            }

        };
                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT

                    ));
                    requestQueue.add(stringRequest);

    }
    public void AppExit()
    {

        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      finish();
    }
    private void showDialognoConnection(){
        final AlertDialog.Builder dialogCon = new AlertDialog.Builder(LoginActivity.this);

        dialogCon.setTitle("Message:");
        dialogCon.setMessage("You must connect to the internet to log in ");


        dialogCon.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogCon.show();
    }
    private void showDialogLoggedin(){
        final AlertDialog.Builder dialogCon = new AlertDialog.Builder(LoginActivity.this);

        dialogCon.setTitle("Message:");
        dialogCon.setMessage("User is successfully logged in");


        dialogCon.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(LoginActivity.this, StoriesActivity.class));
                LoginActivity.this.finish();
                dialog.dismiss();
            }
        });
        dialogCon.show();
    }

}