package com.example.lenovo.storytime.SQLite;


public class StoriesModelSqlite {
    private int id;
    private byte[] cover;
    private byte[] image;
    private String title;
    private String category;
    private String uploadedat;
    private String storycontent;
    private String downloaded;

    public StoriesModelSqlite(int id, byte[] cover, byte[] image, String title, String category, String uploadedat, String storycontent, String downloaded) {
        this.id = id;
        this.cover = cover;
        this.image = image;
        this.title = title;
        this.category = category;
        this.uploadedat = uploadedat;
        this.storycontent = storycontent;
        this.downloaded = downloaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getCover() {
        return cover;
    }

    public void setCover(byte[] cover) {
        this.cover = cover;
    }

    public byte[] getImage() {
        return image;
    }

    public  void setImage(byte[] image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public  void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUploadedat() {
        return uploadedat;
    }

    public void setUploadedat(String uploadedat) {
        this.uploadedat = uploadedat;
    }

    public String getStorycontent() {
        return storycontent;
    }

    public void setStorycontent(String storycontent) {
        this.storycontent = storycontent;
    }

    public String getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(String downloaded) {
        this.downloaded = downloaded;
    }
}