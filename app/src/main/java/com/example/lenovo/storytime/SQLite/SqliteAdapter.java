package com.example.lenovo.storytime.SQLite;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.storytime.Connections.CheckNetwork;
import com.example.lenovo.storytime.LoginandRegister.SharedPrefManager;
import com.example.lenovo.storytime.MySQL.SearchActivity;
import com.example.lenovo.storytime.MySQL.StoriesActivity;
import com.example.lenovo.storytime.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.lenovo.storytime.MySQL.StoriesActivity.imageViewToByte;
import static com.example.lenovo.storytime.MySQL.StoriesActivity.updateStoriesList;


public class SqliteAdapter extends BaseAdapter {

    private Context context;
    private  int layout;
    private ArrayList<StoriesModelSqlite> storiesList;
    String downloaded_holder;


    public SqliteAdapter(Context context, int layout, ArrayList<StoriesModelSqlite> storiesList) {
        this.context = context;
        this.layout = layout;
        this.storiesList = storiesList;

    }


    @Override
    public int getCount() {
        return storiesList.size();
    }

    @Override
    public Object getItem(int position) {
        return storiesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView cover,read;
        TextView title, category,uploadedat,tv_downloaded,tv_id;
        Button delete;

    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View row = view;
        ImageView cover,read;
        TextView title, category,uploadedat,tv_downloaded;
        Button delete;
        ViewHolder holder = new ViewHolder();
        final StoriesModelSqlite stories = storiesList.get(position);

        final String readername = SharedPrefManager.getmInstance(context).getreadername();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.title = (TextView) row.findViewById(R.id.title);
            holder.category = (TextView) row.findViewById(R.id.category);
            holder.uploadedat= (TextView)row.findViewById(R.id.uploadedat);
            holder.cover = (ImageView) row.findViewById(R.id.cover);
            holder.delete = (Button)row.findViewById(R.id.btn_Del);
            holder.read = (ImageView)row.findViewById(R.id.imagebutton);
            holder.tv_downloaded = (TextView)row.findViewById(R.id.tv_downloaded);

            final ViewHolder finalHolder = holder;
            holder.read.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LibraryReadActivity.class);
                    intent.putExtra("downloaded", stories.getDownloaded());
                    intent.putExtra("story_content", stories.getStorycontent());
                    intent.putExtra("title", stories.getTitle());
                    intent.putExtra("image",stories.getImage());
                    context.startActivity(intent);

                }

            });
            row.setTag(holder);

            holder.delete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if(CheckNetwork.isInternetAvailable(context)) //returns true if internet available
                    {
                      if( stories.getCategory().equals("Fairytale")){
                          showDialogDeleteFairytale(storiesList.get(position).getId());
                      }else if (stories.getCategory().equals("Folklore")){
                          showDialogDeleteFolklore(storiesList.get(position).getId());
                      }else if (stories.getCategory().equals("Fantasy")){
                          showDialogDeleteFantasy(storiesList.get(position).getId());
                      }


                    }
                    else
                    {
                        if (SqliteAdapter.this.equals(SearchActivity.adaptersqlite)) {
                            if( SearchActivity.adaptersqlite.storiesList.get(position).getCategory().equals("Fairytale")){
                                showDialogDeleteSearch(storiesList.get(position).getId());
                            }else if (SearchActivity.adaptersqlite.storiesList.get(position).getCategory().equals("Folklore")){
                                showDialogDeleteSearch(storiesList.get(position).getId());

                            }else if (SearchActivity.adaptersqlite.storiesList.get(position).getCategory().equals("Fantasy")){
                                showDialogDeleteSearch(storiesList.get(position).getId());
                            }

                        }
                        else if(SqliteAdapter.this.equals(MyLibraryActivity.adapter)) {

                            if (stories.getCategory().equals("Fairytale")) {
                                showDialogDeleteFairytale(storiesList.get(position).getId());
                            } else if (stories.getCategory().equals("Folklore")) {
                                showDialogDeleteFolklore(storiesList.get(position).getId());

                            } else if (stories.getCategory().equals("Fantasy")) {
                                showDialogDeleteFantasy(storiesList.get(position).getId());
                            }


                        }
                    }

                }
            });
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }


        holder.title.setText(stories.getTitle());
        holder.category.setText(stories.getCategory());
        holder.uploadedat.setText(stories.getUploadedat());
        holder. tv_downloaded.setText("FALSE");
        byte[] storiesImage = stories.getCover();

        Bitmap bitmap = BitmapFactory.decodeByteArray(storiesImage, 0, storiesImage.length);
        holder.cover.setImageBitmap(bitmap);
        byte[] storiesImage1 = stories.getImage();
        return row;

    }

    private void showDialogDeleteFairytale(final int idStory ){
        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

        dialogDelete.setTitle("Warning!");
        dialogDelete.setMessage("Are you sure you want to this delete?");

        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {


                    StoriesActivity.sqLiteHelper.deleteData(idStory);
                    //Toast.makeText(context, "Deleted",Toast.LENGTH_SHORT).show();
//                    title_holder = storiesList.get().toString();
                    downloaded_holder = "FALSE";

                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }


                StoriesActivity.updateStoriesList("Fairytale", SharedPrefManager.getmInstance(context).getreadername());

            }
        });

        dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }

    private void showDialogDeleteFolklore(final int idStory){
        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

        dialogDelete.setTitle("Warning!");
        dialogDelete.setMessage("Are you sure you want to this delete?");

        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {


                    StoriesActivity.sqLiteHelper.deleteData(idStory);
                   // Toast.makeText(context, "Deleted",Toast.LENGTH_SHORT).show();
//                    title_holder = storiesList.get().toString();
                    downloaded_holder = "FALSE";

                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }


                StoriesActivity.updateStoriesList("Folklore", SharedPrefManager.getmInstance(context).getreadername());

            }
        });

        dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }
    private void showDialogDeleteFantasy(final int idStory){
        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

        dialogDelete.setTitle("Warning!");
        dialogDelete.setMessage("Are you sure you want to this delete?");

        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {


                    StoriesActivity.sqLiteHelper.deleteData(idStory);
                    //Toast.makeText(context, "Deleted",Toast.LENGTH_SHORT).show();
//                    title_holder = storiesList.get().toString();
                    downloaded_holder = "FALSE";

                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }


                StoriesActivity.updateStoriesList("Fantasy", SharedPrefManager.getmInstance(context).getreadername());

            }
        });

        dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }
    private void showDialogDeleteSearch(final int idStory){
        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

        dialogDelete.setTitle("Warning!");
        dialogDelete.setMessage("Are you sure you want to this delete?");

        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {


                    StoriesActivity.sqLiteHelper.deleteData(idStory);
                    //Toast.makeText(context, "Deleted",Toast.LENGTH_SHORT).show();
//                    title_holder = storiesList.get().toString();
                    downloaded_holder = "FALSE";

                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }


                StoriesActivity.updateSearchList( SharedPrefManager.getmInstance(context).getreadername());

            }
        });

        dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }
}
