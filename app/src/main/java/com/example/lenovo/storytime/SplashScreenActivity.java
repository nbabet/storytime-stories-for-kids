package com.example.lenovo.storytime;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lenovo.storytime.LoginandRegister.BaseScreen;
import com.example.lenovo.storytime.LoginandRegister.LoginActivity;

public class SplashScreenActivity extends AppCompatActivity {

    CountDownTimer cnt;
    TextView timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        overridePendingTransition(R.anim.slide, R.anim.slideout);
        timer = (TextView)(findViewById(R.id.textView));
        timer.setVisibility(View.INVISIBLE);
        cnt = new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                    Intent intent = new Intent(SplashScreenActivity.this, BaseScreen.class);
                    startActivity(intent);
                  finish();
            }

        }.start();}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
