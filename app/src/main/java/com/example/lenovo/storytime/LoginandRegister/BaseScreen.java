package com.example.lenovo.storytime.LoginandRegister;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lenovo.storytime.MySQL.StoriesActivity;
import com.example.lenovo.storytime.R;

public class BaseScreen extends AppCompatActivity {

    Button signin , signup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_screen);
        overridePendingTransition(R.anim.slide, R.anim.slideout);
        signin = (Button)findViewById(R.id.btn_login);
        signup = (Button)findViewById(R.id.btn_register);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KBZipaDeeDooDah.ttf");
        signup.setTypeface(font);
        signin.setTypeface(font);
        if(SharedPrefManager.getmInstance(getApplicationContext()).isLogged()){
            startActivity(new Intent(BaseScreen.this,StoriesActivity.class));
            finish();
        }
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(BaseScreen.this , LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(BaseScreen.this , RegisterActivity.class);
                startActivity(in);
            }
        });

    }
    public void AppExit()
    {

        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppExit();
    }
}
