package com.example.lenovo.storytime.MySQL;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.lenovo.storytime.LoginandRegister.SharedPrefManager;
import com.example.lenovo.storytime.R;
import com.example.lenovo.storytime.Connections.ipaddress;
import com.example.lenovo.storytime.SQLite.SQLiteHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.StoriesViewHolder> {
    String title;
    private Context mCtx;
    ipaddress ipaddress = new ipaddress();
   // private final String URL_UPDATE = ipaddress.ip_address+"/Storytime/Update.php";
    ArrayList<StoriesModel> storiesList = new ArrayList<>();
    ProgressDialog progressDialog;
    public static SQLiteHelper sqLiteHelper;

    public StoriesAdapter(Context mCtx, List<StoriesModel> storiesList) {
        this.mCtx = mCtx;
        this.storiesList = (ArrayList<StoriesModel>) storiesList;
        progressDialog = new ProgressDialog(mCtx);
    }

    @Override
    public StoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.stories_list, null);
        StoriesViewHolder storiesViewHolder = new StoriesViewHolder(view, mCtx, storiesList);
        return storiesViewHolder;
    }

    @Override
    public void onBindViewHolder(final StoriesViewHolder holder, final int position) {
        final StoriesModel stories = storiesList.get(position);

        Glide.with(mCtx)
                .load(stories.getStory_cover_img())
                .into(holder.imageView);

        holder.tv_title.setText(stories.getTitle());
       if(holder.tv_downloaded.getText().toString().equals( "FALSE")){
              holder.tv_uploadedat.setText("Not in your library");
        }else{
            holder.tv_uploadedat.setText(stories.getUploaded_at());
        }

        holder.tv_category.setText(stories.getCategory());
        holder.tv_downloaded.setText(stories.getDownloaded());
        holder.tv_storycontent.setText(stories.getStory_content());
        holder.tv_storycontent.setVisibility(View.INVISIBLE);
        holder.tv_downloaded.setVisibility(View.INVISIBLE);
        holder.tv_storycontent.setVisibility(View.INVISIBLE);
        if(holder.tv_downloaded.getText().equals("TRUE")){
            holder.tv_downloaded1.setVisibility(View.VISIBLE);
            holder.tv_downloaded1.setBackgroundResource(R.drawable.downloaded);
        }else if (holder.tv_downloaded.getText().equals("FALSE")){
            holder.tv_downloaded1.setVisibility(View.INVISIBLE);
        }
        holder.setItemClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(int pos) {
                Intent intent = new Intent();

            }
        });
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final StoriesModel stories = storiesList.get(position);

                if (holder.tv_downloaded.getText().equals("TRUE")){

                    holder.tv_downloaded.setBackgroundResource(R.drawable.downloaded);
                    final AlertDialog.Builder dialogWarning = new AlertDialog.Builder(mCtx);
                    dialogWarning.setTitle("Warning!");
                    dialogWarning.setMessage("This 'story' is already in your library , would you like to duplicate it ?");
                    dialogWarning.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogduplicate, int which) {
                            final Dialog dialog = new Dialog(mCtx);
                            dialog.setContentView(R.layout.downloadlayout);
                            dialog.setTitle("Update");


                            StoriesActivity.imageViewCover = (ImageView) dialog.findViewById(R.id.dl_storycover);
                            StoriesActivity.image = (ImageView) dialog.findViewById(R.id.dl_storyimg);
                            final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
                            final TextView txt = (TextView) dialog.findViewById(R.id.txt);

                            Glide.with(mCtx)
                                    .load(stories.getStory_cover_img())
                                    .into(StoriesActivity.imageViewCover);
                            Glide.with(mCtx)
                                    .load(stories.getStory_img())
                                    .into(StoriesActivity.image);


                            //visibility
                             StoriesActivity.imageViewCover.setVisibility(View.INVISIBLE);
                             StoriesActivity.image.setVisibility(View.INVISIBLE);

                            // set width for dialog
                            int width = (int) (mCtx.getResources().getDisplayMetrics().widthPixels * 0.95);
                            // set height for dialog
                            int height = (int) (mCtx.getResources().getDisplayMetrics().heightPixels * 0.55);
                            dialog.getWindow().setLayout(width, height);
                            dialog.show();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        StoriesActivity.sqLiteHelper.insertData(
                                                SharedPrefManager.getmInstance(mCtx).getreadername().toString(),
                                                StoriesActivity.imageViewToByte(StoriesActivity.imageViewCover),
                                                StoriesActivity.imageViewToByte(StoriesActivity.image),
                                                holder.tv_title.getText().toString(),
                                                holder.tv_category.getText().toString(),
                                                stories.getUploaded_at(),
                                                holder.tv_storycontent.getText().toString(),
                                                stories.getDownloaded()

                                        );
                                       // Toast.makeText(mCtx, "Download Complete", Toast.LENGTH_SHORT).show();
                                        progressbar.setVisibility(View.INVISIBLE);
                                        txt.setText("Download Complete");
                                        StoriesActivity.downloaded="TRUE";
                                        holder.tv_downloaded.setText("TRUE");
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                StoriesActivity.imageViewCover.setImageResource(R.drawable.check);
                                                StoriesActivity.imageViewCover.setVisibility(View.VISIBLE);
                                                final Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialog.dismiss();
                                                        if(holder.tv_downloaded.getText().equals("TRUE")){
                                                            //holder.download.setBackgroundResource(R.drawable.downloaded);
                                                            //holder.download.setEnabled(false);
                                                            holder.tv_downloaded1.setVisibility(View.VISIBLE);

                                                        }
                                                    }
                                                }, 2000);
                                            }
                                        }, 1000);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 5000);

                        }
                    });

                    dialogWarning.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialogWarning.show();


                }else if (holder.tv_downloaded.getText().equals("FALSE")){
                    final Dialog dialog = new Dialog(mCtx);
                    dialog.setContentView(R.layout.downloadlayout);
                    dialog.setTitle("Update");


                    StoriesActivity.imageViewCover = (ImageView) dialog.findViewById(R.id.dl_storycover);
                    StoriesActivity.image = (ImageView) dialog.findViewById(R.id.dl_storyimg);
                    final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
                    final TextView txt = (TextView) dialog.findViewById(R.id.txt);

                    Glide.with(mCtx)
                            .load(stories.getStory_cover_img())
                            .into(StoriesActivity.imageViewCover);
                    Glide.with(mCtx)
                            .load(stories.getStory_img())
                            .into(StoriesActivity.image);


                    //visibility
                    StoriesActivity.imageViewCover.setVisibility(View.INVISIBLE);
                    StoriesActivity.image.setVisibility(View.INVISIBLE);

                    // set width for dialog
                    int width = (int) (mCtx.getResources().getDisplayMetrics().widthPixels * 0.95);
                    // set height for dialog
                    int height = (int) (mCtx.getResources().getDisplayMetrics().heightPixels * 0.55);
                    dialog.getWindow().setLayout(width, height);
                    dialog.show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                StoriesActivity.sqLiteHelper.insertData(
                                        SharedPrefManager.getmInstance(mCtx).getreadername().toString(),
                                    StoriesActivity.imageViewToByte(StoriesActivity.imageViewCover),
                                    StoriesActivity.imageViewToByte(StoriesActivity.image),
                                        holder.tv_title.getText().toString(),
                                        holder.tv_category.getText().toString(),
                                        stories.getUploaded_at(),
                                        holder.tv_storycontent.getText().toString(),
                                        stories.getDownloaded()
                                );
                               // Toast.makeText(mCtx, "Download Complete", Toast.LENGTH_SHORT).show();
                                progressbar.setVisibility(View.INVISIBLE);
                                txt.setText("Download Complete");
                                StoriesActivity.downloaded="TRUE";
                                holder.tv_downloaded.setText("TRUE");
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        StoriesActivity.imageViewCover.setImageResource(R.drawable.check);
                                        StoriesActivity.imageViewCover.setVisibility(View.VISIBLE);
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialog.dismiss();
                                                if(holder.tv_downloaded.getText().equals("TRUE")){
                                                    //holder.download.setBackgroundResource(R.drawable.downloaded);
                                                    //holder.download.setEnabled(false);
                                                    holder.tv_downloaded1.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }, 2000);
                                    }
                                }, 1000);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 5000);

                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return storiesList.size();
    }

    class StoriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_title, tv_uploadedat, tv_category, tv_downloaded,tv_downloaded1;
        ImageView imageView;
        EditText tv_storycontent;
        Button download;
        ItemClickListener itemClickListener;
        ArrayList<StoriesModel> stories = new ArrayList<StoriesModel>();
        Context mCtx;



        public StoriesViewHolder(View itemView, Context mCtx, ArrayList<StoriesModel> stories) {
            super(itemView);
            this.stories = stories;
            this.mCtx = mCtx;
            tv_title = itemView.findViewById(R.id.tv_Title);
            tv_uploadedat = itemView.findViewById(R.id.tv_uploadedat);
            tv_category = itemView.findViewById(R.id.tv_category);
            imageView = itemView.findViewById(R.id.iv_storycover);
            download = itemView.findViewById(R.id.btn_dl);
            tv_downloaded = itemView.findViewById(R.id.tv_downloaded);
            tv_downloaded1 = itemView.findViewById(R.id.tv_downloaded1);
            tv_storycontent = (itemView).findViewById(R.id.tv_sc);
            itemView.setOnClickListener(this);
            download.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
            int position = getAdapterPosition();
            StoriesModel stories = this.stories.get(position);
            Intent intent = new Intent(this.mCtx, StoriesReadActivity.class);
            intent.putExtra("story_img", stories.getStory_img());
            intent.putExtra("story_content", stories.getStory_content());
            intent.putExtra("title", stories.getTitle());
            this.mCtx.startActivity(intent);
        }

    }
}
