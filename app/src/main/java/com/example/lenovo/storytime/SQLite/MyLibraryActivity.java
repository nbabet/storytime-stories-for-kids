package com.example.lenovo.storytime.SQLite;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.storytime.Connections.CheckNetwork;
import com.example.lenovo.storytime.Connections.ipaddress;
import com.example.lenovo.storytime.LoginandRegister.BaseScreen;
import com.example.lenovo.storytime.LoginandRegister.LoginActivity;
import com.example.lenovo.storytime.LoginandRegister.SharedPrefManager;
import com.example.lenovo.storytime.MySQL.SearchActivity;
import com.example.lenovo.storytime.MySQL.StoriesActivity;
import com.example.lenovo.storytime.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyLibraryActivity extends AppCompatActivity {

    private TextView mTextMessage;
    public static Button btn_fairytale;
    public Button btn_folklore;
    public Button btn_fantasy;
    public Button btn_logout;
    ProgressDialog progressDialog;
    String folder_file_logout = "/Logout.php";
    GridView gridView;
    public static ArrayList<StoriesModelSqlite> list;
    public static SqliteAdapter adapter = null;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_stories:
                    setTitle("Stories");
                    Intent mylibrary = new Intent(MyLibraryActivity.this, StoriesActivity.class);
                    startActivity(mylibrary);
                    return true;

                case R.id.navigation_search:
                    setTitle("Search");
                    Intent search = new Intent(MyLibraryActivity.this, SearchActivity.class);
                    startActivity(search);
                    return true;
                case R.id.navigation_library:
                    setTitle("My Library");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_library);
        overridePendingTransition(R.anim.slide, R.anim.slideout);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        btn_fairytale = (Button) findViewById(R.id.btn_mlfairytale);
        btn_folklore = (Button) findViewById(R.id.btn_mlfolklore);
        btn_fantasy = (Button) findViewById(R.id.btn_mlfantasy);
        btn_logout = (Button)findViewById(R.id.btn_logout);
        overridePendingTransition(R.anim.slide, R.anim.slideout);
        progressDialog = new ProgressDialog(this);

        gridView = (GridView) findViewById(R.id.gridView);
        list = new ArrayList<StoriesModelSqlite>();
        final Dialog dialog = new Dialog(MyLibraryActivity.this);
        dialog.setContentView(R.layout.refreshlayout);
        dialog.setTitle("Message :");
        final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
        final TextView txt = (TextView) dialog.findViewById(R.id.txt);

        int width = (int) (MyLibraryActivity.this.getResources().getDisplayMetrics().widthPixels * 0.95);
        // set height for dialog
        int height = (int) (MyLibraryActivity.this.getResources().getDisplayMetrics().heightPixels * 0.40);
        dialog.getWindow().setLayout(width, height);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetwork.isInternetAvailable(MyLibraryActivity.this)) //returns true if internet available
                {
                    showDialogWarningLogout();
                }
                else
                {
                    //Toast.makeText(MyLibraryActivity.this,"No Internet Connection",1000).show();
                    showDialogLogout();
                }
            }
        });
        loadMyLibrary("Fairytale",SharedPrefManager.getmInstance(getApplicationContext()).getreadername());
        btn_fairytale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_fairytale.setBackgroundColor(Color.parseColor("#c097a3e5"));
                btn_folklore.setBackgroundColor(Color.parseColor("#993b405b"));
                btn_fantasy.setBackgroundColor(Color.parseColor("#993b405b"));
                loadMyLibrary("Fairytale",SharedPrefManager.getmInstance(getApplicationContext()).getreadername());

            }
        });

        btn_folklore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_folklore.setBackgroundColor(Color.parseColor("#c097a3e5"));
                btn_fairytale.setBackgroundColor(Color.parseColor("#993b405b"));
                btn_fantasy.setBackgroundColor(Color.parseColor("#993b405b"));
                loadMyLibrary("Folklore",SharedPrefManager.getmInstance(getApplicationContext()).getreadername());
            }
        });

        btn_fantasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                btn_fantasy.setBackgroundColor(Color.parseColor("#c097a3e5"));
                btn_folklore.setBackgroundColor(Color.parseColor("#993b405b"));
                btn_fairytale.setBackgroundColor(Color.parseColor("#993b405b"));
                loadMyLibrary("Fantasy",SharedPrefManager.getmInstance(getApplicationContext()).getreadername());
            }
        });
    }

    void loadMyLibrary(String categ,String readername){
        // get all data from sqlite
        Cursor cursor = StoriesActivity.sqLiteHelper.getData("SELECT * FROM STORIES WHERE category = '"+categ+"' and readername='"+readername+"'");
        list.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            byte[] cover = cursor.getBlob(2);
            byte[] image = cursor.getBlob(3);
            String title = cursor.getString(4);
            String category = cursor.getString(5);
            String uploadedat = cursor.getString(6);
            String storycontent = cursor.getString(7);
            String downloaded = cursor.getString(8);
            list.add(new StoriesModelSqlite(id,cover,image,title,category,uploadedat,storycontent ,downloaded));
        }

        adapter = new SqliteAdapter(this, R.layout.library_list, list);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (adapter.getCount() == 0 ) {
            showDialognoData();
        }
    }

    private void logout(final String readername){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ipaddress.ip_address + folder_file_logout,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MyLibraryActivity.this,BaseScreen.class));
                        MyLibraryActivity.this.finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(MyLibraryActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("readername", readername);
                return params;
            }

        };
        /////////////////binago
        final Dialog dialog = new Dialog(MyLibraryActivity.this);
        dialog.setContentView(R.layout.refreshlayout);
        dialog.setTitle("Message: 'Slow connection'");


        final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
        final TextView txt = (TextView) dialog.findViewById(R.id.txt);
        // set width for dialog
        int width = (int) (MyLibraryActivity.this.getResources().getDisplayMetrics().widthPixels * 0.95);
        // set height for dialog
        int height = (int) (MyLibraryActivity.this.getResources().getDisplayMetrics().heightPixels * 0.28);
        dialog.getWindow().setLayout(width, height);
        dialog.show();
        txt.setText("Logging out please wait for a while..");

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(MyLibraryActivity.this);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT

            ));
            requestQueue.add(stringRequest);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void showDialogLogout(){
        final AlertDialog.Builder dialogLogout = new AlertDialog.Builder(MyLibraryActivity.this);

        dialogLogout.setTitle("Message:");
        dialogLogout.setMessage("There is no connection, Logout unavalaible");


        dialogLogout.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                MyLibraryActivity.this.finish();

            }
        });
        dialogLogout.show();
    }
    private void showDialogWarningLogout(){
        final AlertDialog.Builder dialogwarning = new AlertDialog.Builder(MyLibraryActivity.this);

        dialogwarning.setTitle("Warning:");
        dialogwarning.setMessage("Are you sure you want to logout?");

        dialogwarning.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout(SharedPrefManager.getmInstance(getApplicationContext()).getreadername());
                SharedPrefManager.getmInstance(getApplicationContext()).logout();
            }
        });
        dialogwarning.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogwarning.show();
    }
    public void AppExit()
    {

        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    finish();
    }

    private void showDialognoData(){
        final AlertDialog.Builder dialognoData = new AlertDialog.Builder(MyLibraryActivity.this);

        dialognoData.setTitle("Message:");
        dialognoData.setMessage("There is no story in this category ");


        dialognoData.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialognoData.show();
    }
}
