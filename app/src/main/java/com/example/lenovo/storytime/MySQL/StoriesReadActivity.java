package com.example.lenovo.storytime.MySQL;

import android.content.Intent;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.lenovo.storytime.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class StoriesReadActivity extends AppCompatActivity implements Animation.AnimationListener {
    Button backbtn,pause,stop,resume,minus,plus;
    EditText storycontent;
    TextView txttitle;
    ImageView iv_storyimg;
    // ArrayList<Stories> storiesList;
    TextToSpeech toSpeech;
    Animation move,move1,move2,back,back1,back2;
    List<String> samp;
    int i;
    String [] words = new String [1000];
    int result;
    Handler handler = new Handler();
    boolean play=false;
    int ctr=0;
    Intent intent;
    String title,story,image_url;
    Thread t;
    boolean started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories_read);

        backbtn = (Button)findViewById(R.id.btn_back);
        plus = (Button)findViewById(R.id.btn_plus);
        minus = (Button)findViewById(R.id.btn_minus);
        pause = (Button)findViewById(R.id.btn_pause);
        resume = (Button)findViewById(R.id.btn_resume);
        stop = (Button)findViewById(R.id.btn_stop);
        storycontent = (EditText) findViewById(R.id.et_content);
        txttitle = (TextView) findViewById(R.id.tv_readtitle);
        iv_storyimg = (ImageView)findViewById(R.id.iv_storyimg);

        intent = getIntent();
        image_url = intent.getStringExtra("story_img");
        title = intent.getStringExtra("title");
        story = intent.getStringExtra("story_content");

        Glide.with(this)
                .load(image_url)
                .into(iv_storyimg);
        txttitle.setText(title);
        storycontent.setText(story);
        samp = new ArrayList<String>(Arrays.asList(getIntent().getStringExtra("story_content").split("\\.|\\,")));

        toSpeech = new TextToSpeech(StoriesReadActivity.this, new TextToSpeech.OnInitListener() {

            public void onInit(int status) {
                if(status==TextToSpeech.SUCCESS){
                    result= toSpeech.setLanguage(Locale.ENGLISH);
                }else{
                    Toast.makeText(getApplicationContext(), "Feature not supported in your device",Toast.LENGTH_SHORT).show();
                }
            }
        });
        toSpeech.setLanguage(Locale.ENGLISH);


        overridePendingTransition(R.anim.slide, R.anim.slideout);
        move = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);
        move.setAnimationListener(this);
        move1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move1);
        move1.setAnimationListener(this);
        move2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move2);
        move2.setAnimationListener(this);

        back = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.back);
        back.setAnimationListener(this);
        back1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.back1);
        back1.setAnimationListener(this);
        back2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.back2);
        back2.setAnimationListener(this);

        //visibility
        minus.setVisibility(View.INVISIBLE);
        pause.setVisibility(View.INVISIBLE);
        resume.setVisibility(View.INVISIBLE);
        stop.setVisibility(View.INVISIBLE);


        //button listeners
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             finish();
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minus.setVisibility(View.VISIBLE);
                pause.setVisibility(View.VISIBLE);
                resume.setVisibility(View.VISIBLE);
                stop.setVisibility(View.VISIBLE);
                plus.setVisibility(View.INVISIBLE);

                stop.startAnimation(move);
                resume.startAnimation(move1);
                pause.startAnimation(move2);

                stopanimation();
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                plus.setVisibility(View.VISIBLE);
                minus.setVisibility(View.INVISIBLE);
                stop.startAnimation(back);
                resume.startAnimation(back1);
                pause.startAnimation(back2);
                gone();
            }
        });

        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!started){
                    started=true;
                    play=true;
                    t.start();
                }
                else{
                    if(!play){
                        play=true;
                    }
                }


            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    play=false;
                    ctr=0;

            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(play) {
                    play=false;
                }
            }
        });

        t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    handler.post(new Runnable(){
                        @Override
                        public void run() {
                            if(play){
                                if(samp.size()>ctr){
                                    readStory(samp.get(ctr));
                                }
                            }
                        }
                    });

                    try{
                        Thread.sleep(100);
                    }catch (InterruptedException e){

                    }
                }
            }
        });
    }

    void readStory(String text){
        try {
            if(!toSpeech.isSpeaking() && samp.size()>ctr){
                HashMap<String, String> params = new HashMap<String, String>();
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,text);
                toSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, params);
                ctr++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopanimation(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pause.clearAnimation();
                stop.clearAnimation();
                resume.clearAnimation();
            }
        }, 500);

    }

    public void gone(){
        stopanimation();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pause.setVisibility(View.INVISIBLE);
                stop.setVisibility(View.INVISIBLE);
                resume.setVisibility(View.INVISIBLE);

            }
        }, 500);

    }
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(toSpeech!=null)
        {
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
