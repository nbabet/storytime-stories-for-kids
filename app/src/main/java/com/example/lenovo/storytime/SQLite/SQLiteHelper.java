package com.example.lenovo.storytime.SQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;


public class SQLiteHelper extends SQLiteOpenHelper {


    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void queryData(String sql){
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }

    public void insertData(String readername, byte[] coverimage, byte[] image, String title, String category, String uploadedat, String storycontent, String downloaded){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO STORIES VALUES (NULL,?, ?, ?, ?, ?, date('now'),?,?)";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, readername);
        statement.bindBlob(2, coverimage);
        statement.bindBlob(3, image);
        statement.bindString(4, title);
        statement.bindString(5, category);
        statement.bindString(6, storycontent);
        statement.bindString(7, downloaded);
        statement.executeInsert();

    }

    public  void deleteData(int id) {
        SQLiteDatabase database = getWritableDatabase();

        String sql = "DELETE FROM STORIES WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        database.close();
    }

    public boolean checkIfDownloaded(String category,String title,String readername){
        SQLiteDatabase database = getReadableDatabase();
        String sql = "SELECT * FROM STORIES WHERE category='"+category+"' and title ='"+title+"' and readername='"+readername+"'"  ;
        Cursor cursor = database.rawQuery(sql, null);
        if(cursor.getCount()>0){
            return  true;
        }
        return false;
    }

    public String getDateDownloaded(String category,String title,String readername){
        SQLiteDatabase database = getReadableDatabase();
        String sql = "SELECT * FROM STORIES WHERE category='"+category+"' and title ='"+title+"' and readername='"+readername+"'"  ;
        Cursor cursor = database.rawQuery(sql, null);
        String str = "";
        while (cursor.moveToNext()) {
            str= cursor.getString(6);
        }

        return str;
    }



    public Cursor getData(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
