package com.example.lenovo.storytime.MySQL;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.storytime.Connections.CheckNetwork;
import com.example.lenovo.storytime.LoginandRegister.SharedPrefManager;
import com.example.lenovo.storytime.R;
import com.example.lenovo.storytime.SQLite.MyLibraryActivity;
import com.example.lenovo.storytime.Connections.ipaddress;
import com.example.lenovo.storytime.SQLite.SQLiteHelper;
import com.example.lenovo.storytime.SQLite.SqliteAdapter;
import com.example.lenovo.storytime.SQLite.StoriesModelSqlite;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    private TextView mTextMessage;
    RecyclerView rv_search;
    public EditText search;
    ipaddress ipaddress = new ipaddress();
    Spinner spinSearchby;
    StoriesAdapter adapter;
    GridView gridView;
    public static ArrayList<StoriesModelSqlite> list;
    public static SqliteAdapter adaptersqlite = null;

    ArrayList<StoriesModel> storiesList = new ArrayList<StoriesModel>();
    private final String URL_STORIES = ipaddress.ip_address + "/Stories.php";
    public static SQLiteHelper sqLiteHelper;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_stories:

                    setTitle("Stories");
                    Intent search = new Intent(SearchActivity.this, StoriesActivity.class);
                    startActivity(search);

                    return true;

                case R.id.navigation_search:

                    setTitle("Search");
                    return true;

                case R.id.navigation_library:

                    setTitle("My Library");
                    Intent mylibrary = new Intent(SearchActivity.this, MyLibraryActivity.class);
                    startActivity(mylibrary);
                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        overridePendingTransition(R.anim.slide, R.anim.slideout);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        rv_search = (RecyclerView) findViewById(R.id.rv_search);
        search = (EditText) findViewById(R.id.search);
        getWindow().setSoftInputMode(WindowManager.
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        rv_search.setLayoutManager(new LinearLayoutManager(this));
        rv_search.setHasFixedSize(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        progressDialog = new ProgressDialog(this);
        list = new ArrayList<StoriesModelSqlite>();
        gridView = (GridView) findViewById(R.id.gridView);
        sqLiteHelper = new SQLiteHelper(this, "storytimeDB1.sqlite", null, 1);
        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS STORIES(id INTEGER PRIMARY KEY AUTOINCREMENT,readername VARCHAR, cover BLOB, image BLOB, title VARCHAR, category VARCHAR, uploadedat VARCHAR, storycontent VARCHAR,downloaded VARCHAR)");
        //Check connection
        if (CheckNetwork.isInternetAvailable(SearchActivity.this)) //returns true if internet available
        {
            loadStories("All");
            gridView.setVisibility(View.INVISIBLE);
        } else {
            rv_search.setVisibility(View.INVISIBLE);
            LoadSqlite(SharedPrefManager.getmInstance(getApplicationContext()).getreadername());


        }
        rv_search.setAdapter(adapter);
        spinSearchby = (Spinner) findViewById(R.id.spinner);

        //spinner
        final String[] searchby = new String[]{
                "Search by..",
                "-----",
                "Title",
                "Date"
        };
        final List<String> searchbyList = new ArrayList<>(Arrays.asList(searchby));

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, searchbyList) {

            @Override
            public boolean isEnabled(int position) {
                if (position == 0 || position == 1) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else if (position == 1) {
                    tv.setTextColor(Color.GRAY);
                    addTextListenerCategory();
                } else if (position == 2) {
                    tv.setTextColor(Color.BLACK);
                    if (CheckNetwork.isInternetAvailable(SearchActivity.this)) //returns true if internet available
                    {
                        addTextListenerTitle();
                    } else {
                       addTextListenerTitleSqlite();
                    }

                } else if (position == 3) {
                    tv.setTextColor(Color.BLACK);
                    if (CheckNetwork.isInternetAvailable(SearchActivity.this)) //returns true if internet available
                    {
                        addTextListenerDate();
                    } else {
                        addTextListenerDateSqlite();
                    }

                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinSearchby.setAdapter(spinnerArrayAdapter);
        spinSearchby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if (position > 0) {
                    Toast.makeText
                            (getApplicationContext(), "Search by : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


       public void addTextListenerTitle() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence charString, int start, int before, int count) {


                final List<StoriesModel> filteredList1 = new ArrayList<StoriesModel>();


                for (StoriesModel stories : storiesList) {

                    if (stories.getTitle().toLowerCase().contains(charString)) {

                        filteredList1.add(stories);
                    }
                }

                rv_search.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                rv_search.setAdapter(adapter);


                adapter = new StoriesAdapter(SearchActivity.this, filteredList1);
                adapter.notifyDataSetChanged();

            }
        });

    }

    public void addTextListenerTitleSqlite() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence charString, int start, int before, int count) {


                final List<StoriesModelSqlite> filteredList1 = new ArrayList<StoriesModelSqlite>();


                for (StoriesModelSqlite storiesModelSqlite : list) {

                    if (storiesModelSqlite.getTitle().toLowerCase().contains(charString)) {

                        filteredList1.add(storiesModelSqlite);
                    }
                }

                //rv_search.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                gridView.setAdapter(adaptersqlite);


                adaptersqlite = new SqliteAdapter(SearchActivity.this,R.layout.library_list, (ArrayList<StoriesModelSqlite>) filteredList1);
                adaptersqlite.notifyDataSetChanged();

            }
        });

    }

    public void addTextListenerDateSqlite() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence charString, int start, int before, int count) {


                final List<StoriesModelSqlite> filteredList1 = new ArrayList<StoriesModelSqlite>();


                for (StoriesModelSqlite storiesModelSqlite : list) {

                    if (storiesModelSqlite.getUploadedat().toLowerCase().contains(charString)) {

                        filteredList1.add(storiesModelSqlite);
                    }
                }

                //rv_search.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                gridView.setAdapter(adaptersqlite);


                adaptersqlite = new SqliteAdapter(SearchActivity.this,R.layout.library_list, (ArrayList<StoriesModelSqlite>) filteredList1);
                adaptersqlite.notifyDataSetChanged();

            }
        });

    }
    public void addTextListenerDate() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence charString, int start, int before, int count) {


                final List<StoriesModel> filteredList2 = new ArrayList<>();


                for (StoriesModel stories : storiesList) {

                    if (stories.getUploaded_at().toLowerCase().contains(charString)) {

                        filteredList2.add(stories);
                    }
                }

                rv_search.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                rv_search.setAdapter(adapter);


                adapter = new StoriesAdapter(SearchActivity.this, filteredList2);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void addTextListenerCategory() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence charString, int start, int before, int count) {


                final List<StoriesModel> filteredList3 = new ArrayList<>();


                for (StoriesModel stories : storiesList) {

                    if (stories.getCategory().toLowerCase().contains(charString)) {

                        filteredList3.add(stories);
                    }
                }

                rv_search.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                rv_search.setAdapter(adapter);

                adapter = new StoriesAdapter(SearchActivity.this, filteredList3);
                adapter.notifyDataSetChanged();

            }
        });
    }

    private void loadStories(final String Category) {
        /////////////////binago
        final Dialog dialog = new Dialog(SearchActivity.this);
        dialog.setContentView(R.layout.refreshlayout);
        dialog.setTitle("Message : 'Slow connection'");
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_STORIES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            storiesList.clear();
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject j = array.getJSONObject(i);
                                if (sqLiteHelper.checkIfDownloaded(j.getString("category"), j.getString("title"), SharedPrefManager.getmInstance(getApplicationContext()).getreadername())) {
                                    storiesList.add(new StoriesModel(
                                            j.getInt("id"),
                                            j.getString("title"),
                                            j.getString("story_cover_img"),
                                            j.getString("story_img"),
                                            j.getString("story_content"),
                                            j.getString("category"),
                                            sqLiteHelper.getDateDownloaded(j.getString("category"), j.getString("title"), SharedPrefManager.getmInstance(getApplicationContext()).getreadername()),
                                            "TRUE"
                                    ));
                                } else {
                                    storiesList.add(new StoriesModel(
                                            j.getInt("id"),
                                            j.getString("title"),
                                            j.getString("story_cover_img"),
                                            j.getString("story_img"),
                                            j.getString("story_content"),
                                            j.getString("category"),
                                            "",
                                            "FALSE"
                                    ));
                                }
                            }

                            StoriesAdapter adapter = new StoriesAdapter(SearchActivity.this, (ArrayList<StoriesModel>) storiesList);
                            rv_search.setAdapter(adapter);
                            try{
                                if(adapter.getItemCount() == 0 || storiesList.size() == 0){
                                    showDialognoData();
                                    dialog.dismiss();
                                }else if (adapter.getItemCount() >= 1){
                                    dialog.dismiss();
                                }

                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        } /*catch (JSONException e) {
                            e.printStackTrace();
                        }*/ catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(SearchActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category", Category);
                return params;
            }
        };
        final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
        final TextView txt = (TextView) dialog.findViewById(R.id.txt);
        // set width for dialog
        int width = (int) (SearchActivity.this.getResources().getDisplayMetrics().widthPixels * 0.95);
        // set height for dialog
        int height = (int) (SearchActivity.this.getResources().getDisplayMetrics().heightPixels * 0.28);
        dialog.getWindow().setLayout(width, height);
        dialog.show();
        txt.setText("Loading data please wait for a while..");

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(SearchActivity.this);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT

            ));
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

////////////////////hanggang dito
    }

    public void LoadSqlite(String readername) {
        // get all data from sqlite
        Cursor cursor = StoriesActivity.sqLiteHelper.getData("SELECT * FROM STORIES WHERE readername = '" + readername + "'");
        SearchActivity.list.clear();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            byte[] cover = cursor.getBlob(2);
            byte[] image = cursor.getBlob(3);
            String title = cursor.getString(4);
            String category = cursor.getString(5);
            String uploadedat = cursor.getString(6);
            String storycontent = cursor.getString(7);
            String downloaded = cursor.getString(8);
            SearchActivity.list.add(new StoriesModelSqlite(id, cover, image, title, category, uploadedat, storycontent, downloaded));
        }
        adaptersqlite = new SqliteAdapter(this, R.layout.library_list, list);
        gridView.setAdapter(adaptersqlite);
        adaptersqlite.notifyDataSetChanged();
        if (adaptersqlite.getCount() == 0 ) {
            showDialognoData();
        }
    }
    public void AppExit()
    {

        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
     finish();
    }
    private void showDialognoData(){
        final AlertDialog.Builder dialognoData = new AlertDialog.Builder(SearchActivity.this);

        dialognoData.setTitle("Message:");
        dialognoData.setMessage("There is no story in this category ");


        dialognoData.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialognoData.show();
    }
}
