package com.example.lenovo.storytime.MySQL;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.storytime.Connections.CheckNetwork;
import com.example.lenovo.storytime.Connections.ipaddress;
import com.example.lenovo.storytime.LoginandRegister.BaseScreen;
import com.example.lenovo.storytime.LoginandRegister.LoginActivity;
import com.example.lenovo.storytime.LoginandRegister.SharedPrefManager;
import com.example.lenovo.storytime.R;
import com.example.lenovo.storytime.SQLite.MyLibraryActivity;
import com.example.lenovo.storytime.SQLite.SQLiteHelper;
import com.example.lenovo.storytime.SQLite.StoriesModelSqlite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StoriesActivity extends AppCompatActivity {


    private TextView mTextMessage;
    ArrayList<StoriesModel> storiesList =  new ArrayList<StoriesModel>();
    RecyclerView recyclerView;
    Button fairytale, folklore,fantasy,btn_logout;
    static ImageView imageViewCover;
    static ImageView image;
    static  String title,category,uploadedat,storycontent,downloaded;
    static EditText story_id;
    // JSON Node names
    ProgressDialog progressDialog;

    ipaddress ipaddress = new ipaddress();
    String folder_file_logout = "/Logout.php";
    //SQLITE
    public static SQLiteHelper sqLiteHelper;

    private final String URL_STORIES = ipaddress.ip_address+"/Stories.php";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_stories:
                    setTitle("Stories");
                    return true;
                case R.id.navigation_search:
                    setTitle("Search");
                    Intent search = new Intent(StoriesActivity.this, SearchActivity.class);
                    startActivity(search);
                    return true;
                case R.id.navigation_library:
                    setTitle("My Library");
                    Intent mylibrary = new Intent(StoriesActivity.this, MyLibraryActivity.class);
                    startActivity(mylibrary);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);
        overridePendingTransition(R.anim.slide, R.anim.slideout);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fairytale = (Button)findViewById(R.id.btn_fairytale);
        folklore = (Button)findViewById(R.id.btn_folklore);
        fantasy = (Button)findViewById(R.id.btn_fantasy);
        btn_logout = (Button)findViewById(R.id.btn_logout);
        progressDialog = new ProgressDialog(this);
        recyclerView = findViewById(R.id.rv_stories);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        sqLiteHelper = new SQLiteHelper(this, "storytimeDB1.sqlite", null, 1);
        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS STORIES(id INTEGER PRIMARY KEY AUTOINCREMENT,readername VARCHAR, cover BLOB, image BLOB, title VARCHAR, category VARCHAR, uploadedat VARCHAR, storycontent VARCHAR,downloaded VARCHAR)");
//dialog loading data

        final Dialog dialog = new Dialog(StoriesActivity.this);
        dialog.setContentView(R.layout.refreshlayout);
        dialog.setTitle("Message :");
        final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
        final TextView txt = (TextView) dialog.findViewById(R.id.txt);

        int width = (int) (StoriesActivity.this.getResources().getDisplayMetrics().widthPixels * 0.95);
        // set height for dialog
        int height = (int) (StoriesActivity.this.getResources().getDisplayMetrics().heightPixels * 0.40);
        dialog.getWindow().setLayout(width, height);

////////////////

        final StoriesAdapter adapter = new StoriesAdapter(StoriesActivity.this, (ArrayList<StoriesModel>) storiesList);

        if(CheckNetwork.isInternetAvailable(StoriesActivity.this)) //returns true if internet available
        {


                loadStories("Fairytale");


        }
        else
        {
            showDialognoConnection();
        }


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetwork.isInternetAvailable(StoriesActivity.this)) //returns true if internet available
                {
                  showDialogWarningLogout();
                }
                else
                {
                    //Toast.makeText(MyLibraryActivity.this,"No Internet Connection",1000).show();
                    showDialogLogout();
                }
            }
        });
        fairytale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                storiesList.clear();
                fairytale.setBackgroundColor(Color.parseColor("#c097a3e5"));
                folklore.setBackgroundColor(Color.parseColor("#993b405b"));
                fantasy.setBackgroundColor(Color.parseColor("#993b405b"));

                if(CheckNetwork.isInternetAvailable(StoriesActivity.this)) //returns true if internet available
                {

                        loadStories("Fairytale");


                }
                else
                {
                    showDialognoConnection();
                }
            }
        });
        folklore.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
               // loadFolklore();
               // clearData();
                storiesList.clear();
                folklore.setBackgroundColor(Color.parseColor("#c097a3e5"));
                fairytale.setBackgroundColor(Color.parseColor("#993b405b"));
                fantasy.setBackgroundColor(Color.parseColor("#993b405b"));
                if(CheckNetwork.isInternetAvailable(StoriesActivity.this)) //returns true if internet available
                {

                        loadStories("Folklore");


                }
                else
                {
                    showDialognoConnection();
                }
            }
        });
        fantasy.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                fantasy.setBackgroundColor(Color.parseColor("#c097a3e5"));
                fairytale.setBackgroundColor(Color.parseColor("#993b405b"));
                folklore.setBackgroundColor(Color.parseColor("#993b405b"));
                if(CheckNetwork.isInternetAvailable(StoriesActivity.this)) //returns true if internet available
                {


                        loadStories("Fantasy");


                }
                else
                {
                    showDialognoConnection();
                }
            }
        });

    }

    private void logout(final String readername){
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, ipaddress.ip_address + folder_file_logout,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(StoriesActivity.this,BaseScreen.class));
                        StoriesActivity.this.finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        //Toast.makeText(StoriesActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("readername", readername);
                return params;
            }
        };
        /////////////////binago
        final Dialog dialog = new Dialog(StoriesActivity.this);
        dialog.setContentView(R.layout.refreshlayout);
        dialog.setTitle("Message: 'Slow connection'");


        final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
        final TextView txt = (TextView) dialog.findViewById(R.id.txt);
        // set width for dialog
        int width = (int) (StoriesActivity.this.getResources().getDisplayMetrics().widthPixels * 0.95);
        // set height for dialog
        int height = (int) (StoriesActivity.this.getResources().getDisplayMetrics().heightPixels * 0.28);
        dialog.getWindow().setLayout(width, height);
        dialog.show();
        txt.setText("Logging out please wait for a while..");

                try {
                    RequestQueue requestQueue = Volley.newRequestQueue(StoriesActivity.this);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT

                    ));
                    requestQueue.add(stringRequest);


                } catch (Exception e) {
                    e.printStackTrace();
                }

    }

    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 888 && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageViewCover.setImageBitmap(bitmap);
                image.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadStories(final String Category) {
        /////////////////binago
        final Dialog dialog = new Dialog(StoriesActivity.this);
        dialog.setContentView(R.layout.refreshlayout);
        dialog.setTitle("Message : 'Slow connection'");
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_STORIES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            storiesList.clear();
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject j = array.getJSONObject(i);
                                if(sqLiteHelper.checkIfDownloaded(j.getString("category"),j.getString("title"),SharedPrefManager.getmInstance(getApplicationContext()).getreadername())){
                                    storiesList.add(new StoriesModel(
                                            j.getInt("id"),
                                            j.getString("title"),
                                            j.getString("story_cover_img"),
                                            j.getString("story_img"),
                                            j.getString("story_content"),
                                            j.getString("category"),
                                            sqLiteHelper.getDateDownloaded(j.getString("category"),j.getString("title"), SharedPrefManager.getmInstance(getApplicationContext()).getreadername()),
                                            "TRUE"
                                    ));
                                }else{
                                    storiesList.add(new StoriesModel(
                                            j.getInt("id"),
                                            j.getString("title"),
                                            j.getString("story_cover_img"),
                                            j.getString("story_img"),
                                            j.getString("story_content"),
                                            j.getString("category"),
                                            "",
                                            "FALSE"
                                    ));
                                }
                            }

                         StoriesAdapter adapter = new StoriesAdapter(StoriesActivity.this, (ArrayList<StoriesModel>) storiesList);
                            recyclerView.setAdapter(adapter);
                            try{
                                if(adapter.getItemCount() == 0 || storiesList.size() == 0){
                                    showDialognoData();
                                    dialog.dismiss();
                                }else if (adapter.getItemCount() >= 1){
                                   dialog.dismiss();
                                }

                            }catch(Exception e){
                                e.printStackTrace();
                            }

                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                       // Toast.makeText(StoriesActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category", Category);
                return params;
            }
        };



        final ProgressBar progressbar = (ProgressBar) dialog.findViewById(R.id.progress);
        final TextView txt = (TextView) dialog.findViewById(R.id.txt);
        // set width for dialog
        int width = (int) (StoriesActivity.this.getResources().getDisplayMetrics().widthPixels * 0.95);
        // set height for dialog
        int height = (int) (StoriesActivity.this.getResources().getDisplayMetrics().heightPixels * 0.28);
        dialog.getWindow().setLayout(width, height);
        dialog.show();
        txt.setText("Loading data please wait for a while..");

                try {
                    RequestQueue requestQueue = Volley.newRequestQueue(StoriesActivity.this);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT

                    ));
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }

////////////////////hanggang dito
    }

 public static void updateStoriesList(String categ, String readername){
        // get all data from sqlite
        Cursor cursor = StoriesActivity.sqLiteHelper.getData("SELECT * FROM STORIES WHERE category = '"+categ+"' and readername='"+readername+"'");
        MyLibraryActivity.list.clear();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            byte[] cover = cursor.getBlob(2);
            byte[] image = cursor.getBlob(3);
            String title = cursor.getString(4);
            String category = cursor.getString(5);
            String uploadedat = cursor.getString(6);
            String storycontent = cursor.getString(7);
            String downloaded = cursor.getString(8);
            MyLibraryActivity.list.add(new StoriesModelSqlite(id,cover,image,title,category,uploadedat,storycontent ,downloaded));
        }

        MyLibraryActivity.adapter.notifyDataSetChanged();

    }
    public static void updateSearchList(String readername){

        Cursor cursor = StoriesActivity.sqLiteHelper.getData("SELECT * FROM STORIES WHERE readername = '"+readername+"'");
        SearchActivity.list.clear();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            byte[] cover = cursor.getBlob(2);
            byte[] image = cursor.getBlob(3);
            String title = cursor.getString(4);
            String category = cursor.getString(5);
            String uploadedat = cursor.getString(6);
            String storycontent = cursor.getString(7);
            String downloaded = cursor.getString(8);
            SearchActivity.list.add(new StoriesModelSqlite(id,cover,image,title,category,uploadedat,storycontent ,downloaded));
        }

        SearchActivity.adaptersqlite.notifyDataSetChanged();
    }
    private void showDialogLogout(){
        final AlertDialog.Builder dialogLogout = new AlertDialog.Builder(StoriesActivity.this);

        dialogLogout.setTitle("Message:");
        dialogLogout.setMessage("There is no connection, Logout unavalaible");


        dialogLogout.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                StoriesActivity.this.finish();
            }
        });
        dialogLogout.show();
    }
    private void showDialognoData(){
        final AlertDialog.Builder dialognoData = new AlertDialog.Builder(StoriesActivity.this);

        dialognoData.setTitle("Message:");
        dialognoData.setMessage("There is no story in this category ");


        dialognoData.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialognoData.show();
    }
    private void showDialognoConnection(){
        final AlertDialog.Builder dialogCon = new AlertDialog.Builder(StoriesActivity.this);

        dialogCon.setTitle("Message:");
        dialogCon.setMessage("There is no connection, can't connect to the server ");


        dialogCon.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogCon.show();
    }
    private void showDialogWarningLogout(){
        final AlertDialog.Builder dialogwarning = new AlertDialog.Builder(StoriesActivity.this);

        dialogwarning.setTitle("Warning:");
        dialogwarning.setMessage("Are you sure you want to logout?");

        dialogwarning.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout(SharedPrefManager.getmInstance(getApplicationContext()).getreadername());
                SharedPrefManager.getmInstance(getApplicationContext()).logout();
            }
        });
        dialogwarning.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogwarning.show();
    }
    public void AppExit()
    {

        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppExit();
    }

}
