package com.example.lenovo.storytime.LoginandRegister;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by lenovo on 2/6/2018.
 */

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "mysharedpref";
    private static final String  KEY_USER_ID= "id";
    private static final String KEY_READERNAME= "readername";
    private static final String KEY_PASSWORD= "password";

    private SharedPrefManager(Context context){
        mCtx = context;
    }
    public static synchronized SharedPrefManager getmInstance(Context context){
        if (mInstance == null){
            mInstance = new SharedPrefManager(context);
        }
        return  mInstance;
    }

    public boolean userLogin(String id,String readername,String password){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_USER_ID,id);
        editor.putString(KEY_READERNAME,readername);
        editor.putString(KEY_PASSWORD,password);

        editor.apply();

        return true;
    }

    public boolean isLogged(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        if(sharedPreferences.getString(KEY_READERNAME,null)!=null){
            return true;
        }
        return false;
    }

    public  boolean logout(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        return true;
    }

    public String getId(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_ID,null);
    }

    public String getreadername(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_READERNAME,null);
    }
    public String getpassword(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PASSWORD,null);
    }

}
