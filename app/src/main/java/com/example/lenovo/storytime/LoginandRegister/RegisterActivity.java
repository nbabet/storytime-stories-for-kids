package com.example.lenovo.storytime.LoginandRegister;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.lenovo.storytime.Connections.CheckNetwork;
import com.example.lenovo.storytime.Connections.ipaddress;
import com.example.lenovo.storytime.MySQL.StoriesActivity;
import com.example.lenovo.storytime.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {


    EditText et_readername, et_password;
    Button btn_register;
    TextView tv_login,tv_signup;
    private static final String folder_file = "/Register.php";
    ProgressDialog progressDialog;
    CheckBox cb;
    ipaddress ipaddress = new ipaddress();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        overridePendingTransition(R.anim.slide, R.anim.slideout);

        et_readername = (EditText) findViewById(R.id.et_readername);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_register = (Button) findViewById(R.id.btn_reg);
        tv_login = (TextView) findViewById(R.id.tv_login);
        CheckBox cb = (CheckBox)findViewById(R.id.cb);
        tv_signup = (TextView)findViewById(R.id.tv_signup);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KBZipaDeeDooDah.ttf");
        tv_login.setTypeface(font);
        tv_signup.setTypeface(font);
        btn_register.setTypeface(font);

        progressDialog = new ProgressDialog(RegisterActivity.this);
        if(CheckNetwork.isInternetAvailable(RegisterActivity.this)) //returns true if internet available
        {

        }
        else
        {
            showDialognoConnection();
        }
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked) {
                    et_password.setInputType(129);

                } else {
                    et_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetwork.isInternetAvailable(RegisterActivity.this)) //returns true if internet available
                {

                    register(et_readername.getText().toString(), et_password.getText().toString());
                }
                else
                {
                    showDialognoConnection();
                }

            }
        });
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(in);

            }
        });

    }

    private void register(final String readername, final String password) {
        if (TextUtils.isEmpty(readername) && readername.length() < 8) {
            et_readername.setError("Please enter username");
            et_readername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password) && password.length() < 8) {
            et_password.setError("Please enter password");
            et_password.requestFocus();
            return;
        }
        if (readername.length() < 8) {
            et_readername.setError("Please enter atleast 8 characters");
            et_readername.requestFocus();
            return;
        }
        if (password.length() < 8) {
            et_password.setError("Please enter atleast 8 characters");
            et_password.requestFocus();
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ipaddress.ip_address + folder_file,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        progressDialog.dismiss();
                        try {
                            JSONObject j = new JSONObject(ServerResponse);
                            if (j.getBoolean("error")) {
                               // Toast.makeText(RegisterActivity.this, "This Account is already existing!", Toast.LENGTH_LONG).show();
                                showDialogExisting();
                            } else {
                               /* SharedPrefManager.getmInstance(getApplicationContext()).userLogin(
                                        j.getString("id"),
                                        j.getString("readername"),
                                        j.getString("password")
                                );*/
                               showDialogRegistered();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        //Toast.makeText(RegisterActivity.this, volleyError.toString().trim(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("readername", readername);
                params.put("password", password);
                return params;
            }
        };

                    RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT

                    ));
                    requestQueue.add(stringRequest);

    }

    public void AppExit() {

        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showDialognoConnection(){
        final AlertDialog.Builder dialogCon = new AlertDialog.Builder(RegisterActivity.this);

        dialogCon.setTitle("Message:");
        dialogCon.setMessage("You must connect to the internet to register");


        dialogCon.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogCon.show();
    }
    private void showDialogRegistered(){
        final AlertDialog.Builder dialogCon = new AlertDialog.Builder(RegisterActivity.this);

        dialogCon.setTitle("Message:");
        dialogCon.setMessage("User is successfully registered");


        dialogCon.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                RegisterActivity.this.finish();
                dialog.dismiss();
            }
        });
        dialogCon.show();
    }
    private void showDialogExisting(){
        final AlertDialog.Builder dialogCon = new AlertDialog.Builder(RegisterActivity.this);

        dialogCon.setTitle("Message:");
        dialogCon.setMessage("User is already existing");


        dialogCon.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        dialogCon.show();
    }

}